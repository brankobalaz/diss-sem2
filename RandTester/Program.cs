﻿using System.Globalization;
using EventSim.RandGenerators;
using EventSim.RandGenerators.Utils;
using EventSim.SimStatistics;

//var e = new ExpRandGenerator(1 / 4.0);
//var e = new UniContinRandGenerator(120, 240);
//var e = new TrianContinRandGenerator(360, 540, 900);

// var attrs = new List<EmpirDiscrRandAttribute>(3);
//
// attrs.Add(new EmpirDiscrRandAttribute(50, 60, 0.2));
// attrs.Add(new EmpirDiscrRandAttribute(61, 100, 0.3));
// attrs.Add(new EmpirDiscrRandAttribute(101, 150, 0.5));
//
// var e = new EmpirDiscrRandGenerator(attrs);

await using var file = new StreamWriter("values.txt");
var count = 5_000_000;

var _seedGenerator = new Random();

var HairServiceTypeRG = new UniContinRandGenerator(0, 1, _seedGenerator.Next());
var SimpleHairServiceTimeMinRG = new UniDiscrRandGenerator(10, 30, _seedGenerator.Next());

var complexAttributes = new List<EmpirDiscrRandAttribute>
{
    new(30, 60, 0.4),
    new(61, 120, 0.6)
};
var ComplexHairServiceTimeMinRG = new EmpirDiscrRandGenerator(complexAttributes, _seedGenerator);

var weddingAttributes = new List<EmpirDiscrRandAttribute>()
{
    new(50, 60, 0.2),
    new(61, 100, 0.3),
    new(101, 150, 0.5)
};
var WeddingHairServiceTimeMinRG = new EmpirDiscrRandGenerator(weddingAttributes, _seedGenerator);

var stat = new DiscreteStat();

for (var i = 0; i < count; i++)
{
    var procedureDuration = -1.0;
    var hairTypeRand = HairServiceTypeRG.Next();

    if (hairTypeRand < 0.4) // Simple hair procedure
        procedureDuration = SimpleHairServiceTimeMinRG!.Next();
    else if (hairTypeRand < 0.4 + 0.4) // Complex hair procedure
        procedureDuration = ComplexHairServiceTimeMinRG!.Next();
    else // Wedding hair procedure
        procedureDuration = WeddingHairServiceTimeMinRG!.Next();

    stat.Add(procedureDuration);

    await file.WriteLineAsync(procedureDuration.ToString("0.000000000000", CultureInfo.InvariantCulture));
}

Console.WriteLine($"95CI: {stat.ConfidenceInterval95.Min} {stat.ConfidenceInterval95.Max}");