﻿using System;

namespace BeautySalon.Models.Utils;

public class ViewModelShell
{
    public ViewModelShell(string title, string description, Type viewModel)
    {
        ViewModel = viewModel;
        Title = title;
        Description = description;
    }

    public string Title { get; }

    public string Description { get; }

    public Type ViewModel { get; }
}