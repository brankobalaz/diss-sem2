﻿using System;
using System.Collections.Generic;

namespace BeautySalon.Models.Utils;

public class Employee : IComparable<Employee>
{
    public bool IsBusy { get; private set; } = false;
    public double ServedTime { get; private set; } = 0;
    private double _lastSeizeTime;

    public void Release(double time)
    {
        IsBusy = false;
        ServedTime += time - _lastSeizeTime;
    }

    public void Seize(double time)
    {
        IsBusy = true;
        _lastSeizeTime = time;
    }

    public double GetUtilization(double atTime)
    {
        if (IsBusy)
            return (ServedTime + (atTime - _lastSeizeTime)) / atTime;

        return ServedTime / atTime;
    }

    public int CompareTo(Employee? other)
    {
        return IsBusy switch
        {
            false when other!.IsBusy => -1,
            true when !other!.IsBusy => 1,
            _ => ServedTime.CompareTo(other.ServedTime)
        };
    }
}