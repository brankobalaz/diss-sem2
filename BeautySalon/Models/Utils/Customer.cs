﻿namespace BeautySalon.Models.Utils;

public class Customer
{
    public ProcedureState HairProcedure { get; set; } = ProcedureState.NotRequested;
    public ProcedureState FaceCleansingProcedure { get; set; } = ProcedureState.NotRequested;
    public ProcedureState MakeupProcedure { get; set; } = ProcedureState.NotRequested;
    public ProcedureState CheckInProcedure { get; set; } = ProcedureState.Requested;
    public ProcedureState PayProcedure { get; set; } = ProcedureState.NotRequested;

    public double EnterSystemTimestamp { get; set; }
    public double EnterSystemClockTimestamp { get; set; }
    
    public double EnterHairQueueTimestamp { get; set; }
    public double EnterFaceQueueTimestamp { get; set; }
    public double EnterPayQueueTimestamp { get; set; }
}