﻿namespace BeautySalon.Models.Utils;

public enum ProcedureState
{
    NotRequested,
    Requested,
    InProgress,
    Done
}