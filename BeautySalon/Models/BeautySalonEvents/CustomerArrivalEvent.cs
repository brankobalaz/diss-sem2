﻿using System;
using BeautySalon.Models.Utils;
using Caliburn.Micro;

namespace BeautySalon.Models.BeautySalonEvents;

public class CustomerArrivalEvent : BeautySalonEvent
{
    public CustomerArrivalEvent(double time, BeautySalonSimulation simulation)
        : base(time, 7, simulation)
    {
    }

    public override void Execute()
    {
        // Spracovanie príchodu
        var newCustomer = new Customer
        {
            EnterSystemTimestamp = Simulation.Time,
            EnterSystemClockTimestamp = Simulation.Time + BeautySalonSimulation.ClockSecondsAtStart
        };

        Simulation.AllCustomers.Add(newCustomer);

        Simulation.AddCustomerToCheckInQueue(newCustomer);
        Simulation.TryStartNewServiceAtReception();

        // Vygenerovanie príchoduďalšieho zákazníka
        var interarrivaTime = Simulation.ArrivalTimeRG!.Next();
        if (Simulation.Time + interarrivaTime <= Simulation.SimulationTimeout)
            Simulation.AddEventToCalendar(new CustomerArrivalEvent(Simulation.Time + interarrivaTime, Simulation));
    }
}