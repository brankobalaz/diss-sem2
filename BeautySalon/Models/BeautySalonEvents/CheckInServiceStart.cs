﻿using System;
using BeautySalon.Models.Utils;

namespace BeautySalon.Models.BeautySalonEvents;

public class CheckInServiceStart : BeautySalonEvent
{
    private const double HairAndFaceProceduresRate = 0.65;
    private const double HairProcedureRate = 0.20;
    private const double MakeupProcedureRate = 0.15;
    private const double FaceCleansingProcedureRate = 0.35;

    private readonly Customer _customer;
    private readonly Employee _employee;

    public CheckInServiceStart(double time, Customer customer, Employee employee, BeautySalonSimulation simulation)
        : base(time, 10, simulation)
    {
        _customer = customer;
        _employee = employee;
    }

    public override void Execute()
    {
        _customer.CheckInProcedure = ProcedureState.InProgress;
        Simulation.OneIterationCustomerInCheckInQueueTimeStat.Add(Time - _customer.EnterSystemTimestamp);
        
        // Spracovanie aktuálneho zákazníka - výber procedúr
        var proceduresRand = Simulation.ProceduresSelectionRG!.Next();
        if (proceduresRand < HairAndFaceProceduresRate) // Hair & face
        {
            _customer.HairProcedure = ProcedureState.Requested;
            _customer.MakeupProcedure = ProcedureState.Requested;
        }
        else if (proceduresRand < HairAndFaceProceduresRate + HairProcedureRate) // Only hair
        {
            _customer.HairProcedure = ProcedureState.Requested;
        }
        else // Only makeup
        {
            _customer.MakeupProcedure = ProcedureState.Requested;
        }

        if (_customer.MakeupProcedure == ProcedureState.Requested)
        {
            var cleansingRand = Simulation.FaceCleansingSelectionRG!.Next();
            if (cleansingRand < FaceCleansingProcedureRate)
                _customer.FaceCleansingProcedure = ProcedureState.Requested;
        }

        _customer.PayProcedure = ProcedureState.Requested;

        // Nastavenie ukončenia obsluhy
        var checkInServiceTime = Simulation.CheckInServiceTimeRG!.Next();
        Simulation.AddEventToCalendar(
            new CheckInServiceEnd(Simulation.Time + checkInServiceTime, _customer, _employee, Simulation));
    }
}