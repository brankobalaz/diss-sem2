﻿using BeautySalon.Models.Utils;

namespace BeautySalon.Models.BeautySalonEvents;

public class MakeupServiceEnd : BeautySalonEvent
{
    private readonly Customer _customer;
    private readonly Employee _employee;

    public MakeupServiceEnd(double time, Customer customer, Employee employee, BeautySalonSimulation simulation)
        : base(time, 3, simulation)
    {
        _customer = customer;
        _employee = employee;
    }

    public override void Execute()
    {
        _employee.Release(Simulation.Time);
        _customer.MakeupProcedure = ProcedureState.Done;

        Simulation.AddCustomerToPayQueue(_customer);
        Simulation.TryStartNewServiceAtReception();

        Simulation.TryStartNewServiceAtFaceStation();
    }
}