﻿using BeautySalon.Models.Utils;

namespace BeautySalon.Models.BeautySalonEvents;

public class SalonClosingEvent : BeautySalonEvent
{
    public SalonClosingEvent(double time, BeautySalonSimulation simulation) : base(time, 100, simulation)
    {
    }

    public override void Execute()
    {
        var leftCustomersCount = 0;
        while (!Simulation.IsToCheckInQueueEmpty)
        {
            leftCustomersCount++;
            Simulation.RemoveCustomerFromCheckInQueue();
        }

        Simulation.SimulationLeftCustomersStat.Add(leftCustomersCount);

        Simulation.SimulationTimeInSystemAtClosingStat.Add(Simulation.OneIterationTimeInSystemStat.Mean());

        Simulation.SimulationCustomerInHairQueueTimeAtClosingStat.Add(
            Simulation.OneIterationCustomerInHairQueueTimeStat.Mean());
        Simulation.SimulationCustomerInFaceQueueTimeAtClosingStat.Add(
            Simulation.OneIterationCustomerInFaceQueueTimeStat.Mean());
        Simulation.SimulationCustomerInPayQueueTimeAtClosingStat.Add(
            Simulation.OneIterationCustomerInPayQueueTimeStat.Mean());

        Simulation.SimulationToFaceQueueLengthAtClosingStat.Add(
            Simulation.OneIterationToFaceQueueLengthStat.Mean(Time));
        
        Simulation.SimulationToHairQueueLengthAtClosingStat.Add(
            Simulation.OneIterationToHairQueueLengthStat.Mean(Time));
        
        Simulation.SimulationToCheckInQueueLengthAtClosingStat.Add(
            Simulation.OneIterationToCheckInQueueLengthStat.Mean(Time));
        Simulation.SimulationToPayQueueLengthAtClosingStat.Add(
            Simulation.OneIterationToPayQueueLengthStat.Mean(Time));
    }
}