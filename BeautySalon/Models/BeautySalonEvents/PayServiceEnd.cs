﻿using BeautySalon.Models.Utils;

namespace BeautySalon.Models.BeautySalonEvents;

public class PayServiceEnd : BeautySalonEvent
{
    private readonly Customer _customer;
    private readonly Employee _employee;

    public PayServiceEnd(double time, Customer customer, Employee employee, BeautySalonSimulation simulation)
        : base(time, 1, simulation)
    {
        _customer = customer;
        _employee = employee;
    }

    public override void Execute()
    {
        _employee.Release(Simulation.Time);
        _customer.PayProcedure = ProcedureState.Done;

        // End of customer in system point !!!
        Simulation.OneIterationTimeInSystemStat.Add(Time - _customer.EnterSystemTimestamp);
        Simulation.OneIterationServedCustomersCount++;

        Simulation.TryStartNewServiceAtReception();
    }
}