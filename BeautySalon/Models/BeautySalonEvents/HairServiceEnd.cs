﻿using System;
using BeautySalon.Models.Utils;

namespace BeautySalon.Models.BeautySalonEvents;

public class HairServiceEnd : BeautySalonEvent
{
    private readonly Customer _customer;
    private readonly Employee _employee;

    public HairServiceEnd(double time, Customer customer, Employee employee, BeautySalonSimulation simulation)
        : base(time, 7, simulation)
    {
        _customer = customer;
        _employee = employee;
    }

    public override void Execute()
    {
        _employee.Release(Simulation.Time);
        _customer.HairProcedure = ProcedureState.Done;

        if (_customer.MakeupProcedure == ProcedureState.Requested)
        {
            Simulation.AddCustomerToFaceQueue(_customer);
            Simulation.TryStartNewServiceAtFaceStation();
        }
        else
        {
            Simulation.AddCustomerToPayQueue(_customer);
            Simulation.TryStartNewServiceAtReception();
        }

        Simulation.TryStartNewServiceAtHairStation();
    }
}