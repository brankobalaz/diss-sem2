﻿using BeautySalon.Models.Utils;

namespace BeautySalon.Models.BeautySalonEvents;

public class PayServiceStart : BeautySalonEvent
{
    private readonly Customer _customer;
    private readonly Employee _employee;

    public PayServiceStart(double time, Customer customer, Employee employee, BeautySalonSimulation simulation)
        : base(time, 2, simulation)
    {
        _customer = customer;
        _employee = employee;
    }

    public override void Execute()
    {
        _customer.PayProcedure = ProcedureState.InProgress;

        var procedureDuration = Simulation.PayServiceTimeRG!.Next();

        Simulation.AddEventToCalendar(
            new PayServiceEnd(Simulation.Time + procedureDuration, _customer, _employee, Simulation));
    }
}