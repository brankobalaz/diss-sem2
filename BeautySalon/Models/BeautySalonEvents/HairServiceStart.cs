﻿using System;
using BeautySalon.Models.Utils;

namespace BeautySalon.Models.BeautySalonEvents;

public class HairServiceStart : BeautySalonEvent
{
    private const double SimpleHairProcedureRate = 0.4;
    private const double ComplexHairProcedureRate = 0.4;
    private const double WeddingHairProcedureRate = 0.2;
    private const int SecondsInMinute = 60;

    private readonly Customer _customer;
    private readonly Employee _employee;

    public HairServiceStart(double time, Customer customer, Employee employee, BeautySalonSimulation simulation)
        : base(time, 8, simulation)
    {
        _customer = customer;
        _employee = employee;
    }

    public override void Execute()
    {
        _customer.HairProcedure = ProcedureState.InProgress;

        // Naplánovanie ukončenia obsluhy
        var hairTypeRand = Simulation.HairServiceTypeRG!.Next();

        var procedureDuration = -1.0;

        if (hairTypeRand < SimpleHairProcedureRate) // Simple hair procedure
            procedureDuration = Simulation.SimpleHairServiceTimeMinRG!.Next() * SecondsInMinute;
        else if (hairTypeRand < SimpleHairProcedureRate + ComplexHairProcedureRate) // Complex hair procedure
            procedureDuration = Simulation.ComplexHairServiceTimeMinRG!.Next() * SecondsInMinute;
        else // Wedding hair procedure
            procedureDuration = Simulation.WeddingHairServiceTimeMinRG!.Next() * SecondsInMinute;

        Simulation.AddEventToCalendar(
            new HairServiceEnd(Simulation.Time + procedureDuration, _customer, _employee, Simulation));

        Simulation.OneIterationHairServiceTimeStat.Add(procedureDuration);
        Simulation.OneIterationHaircutCustomersCount++;

        Simulation.TryStartNewServiceAtReception();
    }
}