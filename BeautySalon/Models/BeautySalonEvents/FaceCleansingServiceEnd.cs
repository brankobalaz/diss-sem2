﻿using BeautySalon.Models.Utils;

namespace BeautySalon.Models.BeautySalonEvents;

public class FaceCleansingServiceEnd : BeautySalonEvent
{
    private readonly Customer _customer;
    private readonly Employee _employee;

    public FaceCleansingServiceEnd(double time, Customer customer, Employee employee, BeautySalonSimulation simulation)
        : base(time, 5, simulation)
    {
        _customer = customer;
        _employee = employee;
    }

    public override void Execute()
    {
        _employee.Release(Simulation.Time);
        _customer.FaceCleansingProcedure = ProcedureState.Done;

        Simulation.AddCustomerToFaceQueue(_customer);
        Simulation.TryStartNewServiceAtFaceStation();
    }
}