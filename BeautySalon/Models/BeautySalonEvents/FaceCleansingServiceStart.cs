﻿using BeautySalon.Models.Utils;

namespace BeautySalon.Models.BeautySalonEvents;

public class FaceCleansingServiceStart : BeautySalonEvent
{
    private readonly Customer _customer;
    private readonly Employee _employee;

    public FaceCleansingServiceStart(double time, Customer customer, Employee employee,
        BeautySalonSimulation simulation)
        : base(time, 6, simulation)
    {
        _customer = customer;
        _employee = employee;
    }

    public override void Execute()
    {
        _customer.FaceCleansingProcedure = ProcedureState.InProgress;

        var procedureDuration = Simulation.FaceCleansingServiceTimeRG!.Next();

        Simulation.AddEventToCalendar(
            new FaceCleansingServiceEnd(Simulation.Time + procedureDuration, _customer, _employee, Simulation));

        Simulation.OneIterationFaceCleansingCustomersCount++;

        Simulation.TryStartNewServiceAtReception();
    }
}