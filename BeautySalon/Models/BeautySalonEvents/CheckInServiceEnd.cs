﻿using System;
using BeautySalon.Models.Utils;

namespace BeautySalon.Models.BeautySalonEvents;

public class CheckInServiceEnd : BeautySalonEvent
{
    private readonly Customer _customer;
    private readonly Employee _employee;

    public CheckInServiceEnd(double time, Customer customer, Employee employee, BeautySalonSimulation simulation)
        : base(time, 9, simulation)
    {
        _customer = customer;
        _employee = employee;
    }

    public override void Execute()
    {
        _employee.Release(Simulation.Time);
        _customer.CheckInProcedure = ProcedureState.Done;
        Simulation.CheckInCustomersInProgressCount--;

        // Spracovanie aktuálneho používateľa
        if (_customer.HairProcedure == ProcedureState.Requested) // Chce vlasy?
        {
            Simulation.AddCustomerToHairQueue(_customer);
            Simulation.TryStartNewServiceAtHairStation();
        }
        else // Tvár chce určite
        {
            Simulation.AddCustomerToFaceQueue(_customer);
            Simulation.TryStartNewServiceAtFaceStation();
        }

        // Spracovanie ďalšieho používateľa
        Simulation.TryStartNewServiceAtReception();
    }
}