﻿using EventSim;

namespace BeautySalon.Models.BeautySalonEvents;

public abstract class BeautySalonEvent : CoreEvent
{
    protected BeautySalonSimulation Simulation { get; }

    protected BeautySalonEvent(double time, int priority, BeautySalonSimulation simulation)
        : base(time, priority)
    {
        Simulation = simulation;
    }
}