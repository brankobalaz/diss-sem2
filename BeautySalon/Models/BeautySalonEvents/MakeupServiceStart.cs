﻿using BeautySalon.Models.Utils;

namespace BeautySalon.Models.BeautySalonEvents;

public class MakeupServiceStart : BeautySalonEvent
{
    private const double SimpleMakeupProceduresRate = 0.30;
    private const double ComplexMakeupProcedureRate = 0.70;
    private const int SecondsInMinute = 60;

    private readonly Customer _customer;
    private readonly Employee _employee;

    public MakeupServiceStart(double time, Customer customer, Employee employee, BeautySalonSimulation simulation)
        : base(time, 4, simulation)
    {
        _customer = customer;
        _employee = employee;
    }

    public override void Execute()
    {
        _customer.MakeupProcedure = ProcedureState.InProgress;

        var procedureTypeRand = Simulation.MakeupServiceTypeRG!.Next();

        var procedureDuration = -1.0;
        if (procedureTypeRand < ComplexMakeupProcedureRate) // Complex makeup
            procedureDuration = Simulation.ComplexMakeupServiceTimeMinRG!.Next() * SecondsInMinute;
        else // Simple makeup
            procedureDuration = Simulation.SimpleMakeupServiceTimeMinRG!.Next() * SecondsInMinute;

        Simulation.AddEventToCalendar(
            new MakeupServiceEnd(Simulation.Time + procedureDuration, _customer, _employee, Simulation));

        Simulation.OneIterationMakeupCustomersCount++;

        Simulation.TryStartNewServiceAtReception();
    }
}