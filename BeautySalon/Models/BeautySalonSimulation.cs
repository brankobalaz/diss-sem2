﻿using System;
using System.Collections.Generic;
using System.Linq;
using BeautySalon.Models.BeautySalonEvents;
using BeautySalon.Models.Utils;
using EventSim;
using EventSim.RandGenerators;
using EventSim.RandGenerators.Utils;
using EventSim.SimStatistics;

namespace BeautySalon.Models;

public class BeautySalonSimulation : EventSimCore
{
    public const double ClockSecondsAtStart = 9 * 60 * 60;

    // For simple usage in GUI
    public List<Customer> AllCustomers { get; } = new();
    public List<Employee> FaceEmployeesStable { get; } = new();
    public List<Employee> HairEmployeesStable { get; } = new();
    public List<Employee> ReceptionEmployeesStable { get; } = new();

    // Usage for employee select
    private readonly List<Employee> _faceEmployeesForSort = new();
    private readonly List<Employee> _hairEmployeesForSort = new();
    private readonly List<Employee> _receptionEmployeesForSort = new();

    public int ReceptionistsCount { get; set; }
    public int HairdressersCount { get; set; }
    public int FaceCaresCount { get; set; }

    // Queues
    public LinkedList<Customer> ToFaceQueue { get; } = new();
    public LinkedList<Customer> ToHairQueue { get; } = new();
    public LinkedList<Customer> ToCheckInQueue { get; } = new();
    public LinkedList<Customer> ToPayQueue { get; } = new();

    public bool IsToFaceQueueEmpty => ToFaceQueue.Count == 0;
    public bool IsToHairQueueEmpty => ToHairQueue.Count == 0;
    public bool IsToPayQueueEmpty => ToPayQueue.Count == 0;
    public bool IsToCheckInQueueEmpty => ToCheckInQueue.Count == 0;

    public int CheckInCustomersInProgressCount { get; set; } = 0;

    public bool IsSpaceForNextCustomer => ToHairQueue.Count + ToFaceQueue.Count + CheckInCustomersInProgressCount <= 10;

    // Random generators
    private Random? _seedGenerator;

    public RandGenerator? ArrivalTimeRG { get; private set; }

    public RandGenerator? CheckInServiceTimeRG { get; private set; } // Registrácia požiadaviek
    public RandGenerator? PayServiceTimeRG { get; private set; }
    public RandGenerator? ProceduresSelectionRG { get; private set; }
    public RandGenerator? FaceCleansingSelectionRG { get; private set; }

    public RandGenerator? HairServiceTypeRG { get; private set; }
    public RandGenerator? SimpleHairServiceTimeMinRG { get; private set; }
    public RandGenerator? ComplexHairServiceTimeMinRG { get; private set; }
    public RandGenerator? WeddingHairServiceTimeMinRG { get; private set; }

    public RandGenerator? MakeupServiceTypeRG { get; private set; }
    public RandGenerator? SimpleMakeupServiceTimeMinRG { get; private set; }
    public RandGenerator? ComplexMakeupServiceTimeMinRG { get; private set; }

    public RandGenerator? FaceCleansingServiceTimeRG { get; private set; }

    // Statisctics
    private List<Statistics> OneIterationStatistics { get; } = new();
    private List<Statistics> SimulationStatistics { get; } = new();

    public DiscreteStat OneIterationTimeInSystemStat { get; } = new();
    public DiscreteStat SimulationTimeInSystemStat { get; } = new();
    public DiscreteStat SimulationTimeInSystemAtClosingStat { get; } = new();

    public int OneIterationServedCustomersCount { get; set; }
    public DiscreteStat SimulationServedCustomersCountStat { get; } = new();
    public DiscreteStat SimulationLeftCustomersStat { get; } = new();

    public DiscreteStat OneIterationCustomerInCheckInQueueTimeStat { get; } = new();
    public DiscreteStat SimulationCustomerInCheckInQueueTimeStat { get; } = new();

    public DiscreteStat OneIterationCustomerInFaceQueueTimeStat { get; } = new();
    public DiscreteStat SimulationCustomerInFaceQueueTimeAtClosingStat { get; } = new();

    public DiscreteStat OneIterationCustomerInHairQueueTimeStat { get; } = new();
    public DiscreteStat SimulationCustomerInHairQueueTimeAtClosingStat { get; } = new();

    public DiscreteStat OneIterationCustomerInPayQueueTimeStat { get; } = new();
    public DiscreteStat SimulationCustomerInPayQueueTimeAtClosingStat { get; } = new();

    //  Queue lenght
    public ContinuousStat OneIterationToFaceQueueLengthStat { get; } = new();
    public ContinuousStat OneIterationToHairQueueLengthStat { get; } = new();
    public ContinuousStat OneIterationToCheckInQueueLengthStat { get; } = new();
    public ContinuousStat OneIterationToPayQueueLengthStat { get; } = new();

    public DiscreteStat SimulationToFaceQueueLengthAtClosingStat { get; } = new();
    public DiscreteStat SimulationToHairQueueLengthAtClosingStat { get; } = new();
    public DiscreteStat SimulationToCheckInQueueLengthAtClosingStat { get; } = new();
    public DiscreteStat SimulationToPayQueueLengthAtClosingStat { get; } = new();

    public DiscreteStat SimulationToHairQueueLengthAtModelEndStat { get; } = new();

    public DiscreteStat OneIterationHairServiceTimeStat { get; } = new();
    public DiscreteStat SimulationHairServiceTimeStat { get; } = new();

    public int OneIterationHaircutCustomersCount { get; set; }
    public int OneIterationMakeupCustomersCount { get; set; }
    public int OneIterationFaceCleansingCustomersCount { get; set; }

    public DiscreteStat SimulationHaircutCustomersCountStat { get; } = new();
    public DiscreteStat SimulationMakeupCustomersCountStat { get; } = new();
    public DiscreteStat SimulationFaceCleansingCustomersCountStat { get; } = new();

    public DiscreteStat SimulationModelCoolingTimeStat { get; } = new();

    // Misc
    public event Action? OnAfterEvent;
    public event Action? OnAfterOneIteration;
    public double SimulationTimeout { get; set; }

    public BeautySalonSimulation()
    {
        OneIterationStatistics.Add(OneIterationTimeInSystemStat);
        OneIterationStatistics.Add(OneIterationCustomerInCheckInQueueTimeStat);
        OneIterationStatistics.Add(OneIterationCustomerInHairQueueTimeStat);
        OneIterationStatistics.Add(OneIterationToFaceQueueLengthStat);
        OneIterationStatistics.Add(OneIterationToHairQueueLengthStat);
        OneIterationStatistics.Add(OneIterationToCheckInQueueLengthStat);
        OneIterationStatistics.Add(OneIterationToPayQueueLengthStat);
        OneIterationStatistics.Add(OneIterationHairServiceTimeStat);

        SimulationStatistics.Add(SimulationTimeInSystemStat);
        SimulationStatistics.Add(SimulationTimeInSystemAtClosingStat);
        SimulationStatistics.Add(SimulationServedCustomersCountStat);
        SimulationStatistics.Add(SimulationCustomerInCheckInQueueTimeStat);
        SimulationStatistics.Add(SimulationCustomerInHairQueueTimeAtClosingStat);
        SimulationStatistics.Add(SimulationToFaceQueueLengthAtClosingStat);
        SimulationStatistics.Add(SimulationToHairQueueLengthAtClosingStat);
        SimulationStatistics.Add(SimulationToCheckInQueueLengthAtClosingStat);
        SimulationStatistics.Add(SimulationToPayQueueLengthAtClosingStat);
        SimulationStatistics.Add(SimulationHairServiceTimeStat);
        SimulationStatistics.Add(SimulationHaircutCustomersCountStat);
        SimulationStatistics.Add(SimulationMakeupCustomersCountStat);
        SimulationStatistics.Add(SimulationFaceCleansingCustomersCountStat);
        SimulationStatistics.Add(SimulationModelCoolingTimeStat);
        SimulationStatistics.Add(SimulationLeftCustomersStat);
    }

    public void InitializeSimulation(double simulationTimeout, int receptionistsCount, int hairdressersCount,
        int faceCaresCount, int seed = -1)
    {
        _seedGenerator = seed == -1 ? new Random() : new Random(seed);

        SimulationTimeout = simulationTimeout;

        ReceptionistsCount = receptionistsCount;
        HairdressersCount = hairdressersCount;
        FaceCaresCount = faceCaresCount;
    }

    public void AddCustomerToCheckInQueue(Customer customer)
    {
        ToCheckInQueue.AddLast(customer);
        OneIterationToCheckInQueueLengthStat.Add(ToCheckInQueue.Count, Time);
    }

    public void AddCustomerToHairQueue(Customer customer)
    {
        customer.EnterHairQueueTimestamp = Time;
        ToHairQueue.AddLast(customer);
        OneIterationToHairQueueLengthStat.Add(ToHairQueue.Count, Time);
    }

    public void AddCustomerToFaceQueue(Customer customer)
    {
        customer.EnterFaceQueueTimestamp = Time;
        ToFaceQueue.AddLast(customer);
        OneIterationToFaceQueueLengthStat.Add(ToFaceQueue.Count, Time);
    }

    public void AddCustomerToPayQueue(Customer customer)
    {
        customer.EnterPayQueueTimestamp = Time;
        ToPayQueue.AddLast(customer);
        OneIterationToPayQueueLengthStat.Add(ToPayQueue.Count, Time);
    }

    public Customer RemoveCustomerFromCheckInQueue()
    {
        var customer = ToCheckInQueue.First?.Value;
        if (customer == null) throw new ArgumentException("There are no customers in queue!");

        ToCheckInQueue.RemoveFirst();

        OneIterationToCheckInQueueLengthStat.Add(ToCheckInQueue.Count, Time);

        return customer;
    }

    private Customer RemoveCustomerFromHairQueue()
    {
        var customer = ToHairQueue.First?.Value;
        if (customer == null) throw new ArgumentException("There are no customers in queue!");

        ToHairQueue.RemoveFirst();

        OneIterationToHairQueueLengthStat.Add(ToHairQueue.Count, Time);
        OneIterationCustomerInHairQueueTimeStat.Add(Time - customer.EnterHairQueueTimestamp);

        return customer;
    }

    private Customer RemoveCustomerFromFaceQueue()
    {
        var customer = ToFaceQueue.First?.Value;
        if (customer == null) throw new ArgumentException("There are no customers in queue!");

        ToFaceQueue.RemoveFirst();

        OneIterationToFaceQueueLengthStat.Add(ToFaceQueue.Count, Time);
        OneIterationCustomerInFaceQueueTimeStat.Add(Time - customer.EnterFaceQueueTimestamp);

        return customer;
    }

    private Customer RemoveCustomerFromPayQueue()
    {
        var customer = ToPayQueue.First?.Value;
        if (customer == null) throw new ArgumentException("There are no customers in queue!");

        ToPayQueue.RemoveFirst();

        OneIterationToPayQueueLengthStat.Add(ToPayQueue.Count, Time);
        OneIterationCustomerInPayQueueTimeStat.Add(Time - customer.EnterPayQueueTimestamp);

        return customer;
    }

    private Employee? GetNotBusyReceptionEmployee()
    {
        _receptionEmployeesForSort.Sort();
        return _receptionEmployeesForSort[0].IsBusy ? null : _receptionEmployeesForSort[0];
    }

    private Employee? GetNotBusyHairEmployee()
    {
        _hairEmployeesForSort.Sort();
        return _hairEmployeesForSort[0].IsBusy ? null : _hairEmployeesForSort[0];
    }

    private Employee? GetNotBusyFaceEmployee()
    {
        _faceEmployeesForSort.Sort();
        return _faceEmployeesForSort[0].IsBusy ? null : _faceEmployeesForSort[0];
    }

    protected override void AfterEventExecute()
    {
        OnAfterEvent?.Invoke();
    }

    protected override void BeforeOneReplication()
    {
        AllCustomers.Clear();
        ToCheckInQueue.Clear();
        ToHairQueue.Clear();
        ToFaceQueue.Clear();
        ToPayQueue.Clear();

        InitializeRandomness();
        InitializeEmployees();

        OneIterationStatistics.ForEach(stat => stat.Clear());

        OneIterationServedCustomersCount = 0;
        OneIterationHaircutCustomersCount = 0;
        OneIterationMakeupCustomersCount = 0;
        OneIterationFaceCleansingCustomersCount = 0;

        CheckInCustomersInProgressCount = 0;

        Time = 0;

        var firstCustomerArrivalTime = ArrivalTimeRG!.Next();
        AddEventToCalendar(new CustomerArrivalEvent(firstCustomerArrivalTime, this));
        AddEventToCalendar(new SalonClosingEvent(SimulationTimeout, this));
    }

    private void InitializeRandomness()
    {
        ArrivalTimeRG = new ExpRandGenerator(1.0 / 450.0, _seedGenerator!.Next());
        CheckInServiceTimeRG = new UniContinRandGenerator(80, 320, _seedGenerator.Next());
        PayServiceTimeRG = new UniContinRandGenerator(130, 230, _seedGenerator.Next());
        ProceduresSelectionRG = new UniContinRandGenerator(0, 1, _seedGenerator.Next());
        FaceCleansingSelectionRG = new UniContinRandGenerator(0, 1, _seedGenerator.Next());
        HairServiceTypeRG = new UniContinRandGenerator(0, 1, _seedGenerator.Next());
        SimpleHairServiceTimeMinRG = new UniDiscrRandGenerator(10, 30, _seedGenerator.Next());

        var complexAttributes = new List<EmpirDiscrRandAttribute>
        {
            new(30, 60, 0.4),
            new(61, 120, 0.6)
        };
        ComplexHairServiceTimeMinRG = new EmpirDiscrRandGenerator(complexAttributes, _seedGenerator);

        var weddingAttributes = new List<EmpirDiscrRandAttribute>()
        {
            new(50, 60, 0.2),
            new(61, 100, 0.3),
            new(101, 150, 0.5)
        };
        WeddingHairServiceTimeMinRG = new EmpirDiscrRandGenerator(weddingAttributes, _seedGenerator);

        MakeupServiceTypeRG = new UniContinRandGenerator(0, 1, _seedGenerator.Next());
        SimpleMakeupServiceTimeMinRG = new UniDiscrRandGenerator(10, 25, _seedGenerator.Next());
        ComplexMakeupServiceTimeMinRG = new UniDiscrRandGenerator(20, 100, _seedGenerator.Next());
        FaceCleansingServiceTimeRG = new TrianContinRandGenerator(360, 540, 900, _seedGenerator.Next());
    }

    private void InitializeEmployees()
    {
        FaceEmployeesStable.Clear();
        HairEmployeesStable.Clear();
        ReceptionEmployeesStable.Clear();

        _faceEmployeesForSort.Clear();
        _hairEmployeesForSort.Clear();
        _receptionEmployeesForSort.Clear();

        for (var i = 0; i < FaceCaresCount; i++)
        {
            var employee = new Employee();
            _faceEmployeesForSort.Add(employee);
            FaceEmployeesStable.Add(employee);
        }

        for (var i = 0; i < HairdressersCount; i++)
        {
            var employee = new Employee();
            _hairEmployeesForSort.Add(employee);
            HairEmployeesStable.Add(employee);
        }

        for (var i = 0; i < ReceptionistsCount; i++)
        {
            var employee = new Employee();
            _receptionEmployeesForSort.Add(employee);
            ReceptionEmployeesStable.Add(employee);
        }
    }

    protected override void BeforeSimulation()
    {
        SimulationStatistics.ForEach(stat => stat.Clear());
    }

    protected override void AfterOneReplication()
    {
        SimulationTimeInSystemStat.Add(OneIterationTimeInSystemStat.Mean());

        SimulationCustomerInCheckInQueueTimeStat.Add(OneIterationCustomerInCheckInQueueTimeStat.Mean());

        SimulationHairServiceTimeStat.Add(OneIterationHairServiceTimeStat.Mean());

        SimulationServedCustomersCountStat.Add(OneIterationServedCustomersCount);
        SimulationHaircutCustomersCountStat.Add(OneIterationHaircutCustomersCount);
        SimulationMakeupCustomersCountStat.Add(OneIterationMakeupCustomersCount);
        SimulationFaceCleansingCustomersCountStat.Add(OneIterationFaceCleansingCustomersCount);

        SimulationModelCoolingTimeStat.Add(Time - SimulationTimeout);

        SimulationToHairQueueLengthAtModelEndStat.Add(OneIterationToHairQueueLengthStat.Mean(Time));

        OnAfterOneIteration?.Invoke();
    }

    public bool TryStartNewServiceAtReception()
    {
        if (!IsToPayQueueEmpty) // Platiaci zákazníci majú prednosť
        {
            var receptionEmployee = GetNotBusyReceptionEmployee();
            if (receptionEmployee is null) // Nie je voľný zamestnanec - nezoberie ani z check in radu
                return false;

            receptionEmployee.Seize(Time);
            var customer = RemoveCustomerFromPayQueue();

            AddEventToCalendar(new PayServiceStart(Time, customer, receptionEmployee, this));
            return true;
        }

        if (!IsSpaceForNextCustomer)
            return false;

        if (!IsToCheckInQueueEmpty)
        {
            var receptionEmployee = GetNotBusyReceptionEmployee();
            if (receptionEmployee is null) // Opakuje sa to - kôli optimalizácií (volanie sort v gettri)
                return false;

            receptionEmployee.Seize(Time);
            var customer = RemoveCustomerFromCheckInQueue();

            CheckInCustomersInProgressCount++;
            AddEventToCalendar(new CheckInServiceStart(Time, customer, receptionEmployee, this));
            return true;
        }

        return false;
    }

    public bool TryStartNewServiceAtFaceStation()
    {
        if (IsToFaceQueueEmpty)
            return false;

        var notBusyEmployee = GetNotBusyFaceEmployee();
        if (notBusyEmployee is not null)
        {
            notBusyEmployee.Seize(Time);
            var customerFromQueue = RemoveCustomerFromFaceQueue();

            if (customerFromQueue.FaceCleansingProcedure == ProcedureState.Requested)
                AddEventToCalendar(
                    new FaceCleansingServiceStart(Time, customerFromQueue, notBusyEmployee, this));
            else
                AddEventToCalendar(
                    new MakeupServiceStart(Time, customerFromQueue, notBusyEmployee, this));

            return true;
        }

        return false;
    }

    public bool TryStartNewServiceAtHairStation()
    {
        if (IsToHairQueueEmpty)
            return false;

        var notBusyEmployee = GetNotBusyHairEmployee();
        if (notBusyEmployee is not null)
        {
            notBusyEmployee.Seize(Time);
            var customerFromQueue = RemoveCustomerFromHairQueue();

            AddEventToCalendar(
                new HairServiceStart(Time, customerFromQueue, notBusyEmployee, this));

            return true;
        }

        return false;
    }

    public double GetReceptionEmployeesUtilization()
    {
        var sum = ReceptionEmployeesStable.Sum(x => x.GetUtilization(Time));
        return sum / ReceptionistsCount;
    }

    public double GetHairEmployeesUtilization()
    {
        var sum = HairEmployeesStable.Sum(x => x.GetUtilization(Time));
        return sum / HairdressersCount;
    }

    public double GetFaceEmployeesUtilization()
    {
        var sum = FaceEmployeesStable.Sum(x => x.GetUtilization(Time));
        return sum / FaceCaresCount;
    }
}