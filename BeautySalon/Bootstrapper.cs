﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using System.Windows;
using System.Windows.Markup;
using BeautySalon.Models;
using BeautySalon.ViewModels;
using Caliburn.Micro;

namespace BeautySalon;

public class Bootstrapper : BootstrapperBase
{
    private SimpleContainer _container = new();

    public Bootstrapper()
    {
        Initialize();
    }

    protected override void Configure()
    {
        _container = new SimpleContainer();

        _container.Instance(_container);

        _container
            .Singleton<IWindowManager, WindowManager>()
            .Singleton<BeautySalonSimulation>();

        _container
            .PerRequest<ShellViewModel>()
            .PerRequest<BeautySalonViewModel>()
            .PerRequest<HairdressersExperimentViewModel>();
    }

    protected override void OnStartup(object sender, StartupEventArgs e)
    {
        Thread.CurrentThread.CurrentCulture = new CultureInfo("sk-SK");
        Thread.CurrentThread.CurrentUICulture = new CultureInfo("sk-SK");

        FrameworkElement.LanguageProperty.OverrideMetadata(typeof(FrameworkElement), new FrameworkPropertyMetadata(
            XmlLanguage.GetLanguage(CultureInfo.CurrentCulture.IetfLanguageTag)));

        DisplayRootViewFor<ShellViewModel>();
    }

    protected override object GetInstance(Type service, string key)
    {
        return _container.GetInstance(service, key);
    }

    protected override IEnumerable<object> GetAllInstances(Type service)
    {
        return _container.GetAllInstances(service);
    }

    protected override void BuildUp(object instance)
    {
        _container.BuildUp(instance);
    }
}