﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Accessibility;
using BeautySalon.Models;
using Caliburn.Micro;
using OxyPlot;
using OxyPlot.Axes;
using OxyPlot.Series;

namespace BeautySalon.ViewModels;

public class HairdressersExperimentViewModel : Screen, IDisposable
{
    private const int MinTimeBetweenGuiUpdate = 70 * 10_000; // 70 ms
    private const double SimulationTimeout = 8 * 60 * 60;

    private readonly BeautySalonSimulation _beautySalonSimulation;
    public PlotModel ExperimentGraph { get; } = new();
    private readonly ScatterErrorSeries _graphValues = new() {ErrorBarColor = OxyColors.Blue};
    private int _currentHairdressers;
    private int _replicationsCount;
    private int _faceCaresCount;
    private int _receptionistsCount;
    private int _hairdressersMaxCount;
    private int _hairdressersMinCount;
    private double _lastGuiUpdateTime = 0;
    private bool _isUiEnabled = true;

    public bool IsUiDisabled => !IsUiEnabled;

    public bool IsUiEnabled
    {
        get => _isUiEnabled;
        set
        {
            _isUiEnabled = value;
            NotifyOfPropertyChange();
            NotifyOfPropertyChange(nameof(IsUiDisabled));
        }
    }

    public int ReplicationsCount
    {
        get => _replicationsCount;
        set
        {
            _replicationsCount = value;
            NotifyOfPropertyChange();
        }
    }

    public int FaceCaresCount
    {
        get => _faceCaresCount;
        set
        {
            _faceCaresCount = value;
            NotifyOfPropertyChange();
        }
    }

    public int ReceptionistsCount
    {
        get => _receptionistsCount;
        set
        {
            _receptionistsCount = value;
            NotifyOfPropertyChange();
        }
    }

    public int HairdressersMaxCount
    {
        get => _hairdressersMaxCount;
        set
        {
            _hairdressersMaxCount = value;
            NotifyOfPropertyChange();
        }
    }

    public int HairdressersMinCount
    {
        get => _hairdressersMinCount;
        set
        {
            _hairdressersMinCount = value;
            NotifyOfPropertyChange();
        }
    }

    public HairdressersExperimentViewModel(BeautySalonSimulation beautySalonSimulation)
    {
        _beautySalonSimulation = beautySalonSimulation;
        _beautySalonSimulation.OnAfterOneIteration += RefreshGui;

        ExperimentGraph.Title = "Závislosť dĺžky fronty na objednanie od počtu kaderníčok";
        ExperimentGraph.Axes.Add(new LinearAxis()
            {Position = AxisPosition.Bottom, Title = "Počet kaderníčok", MinimumMinorStep = 1, MinimumMajorStep = 1});
        ExperimentGraph.Axes.Add(new LinearAxis()
        {
            Position = AxisPosition.Left, Title = "Dĺžka frontu na recepcií", MinimumPadding = 0.3,
            MaximumPadding = 0.3
        });
        ExperimentGraph.Series.Add(_graphValues);

        ExperimentGraph.InvalidatePlot(true);

        ReplicationsCount = 500;

        FaceCaresCount = 6;
        ReceptionistsCount = 2;
        HairdressersMinCount = 1;
        HairdressersMaxCount = 10;
    }

    public async void StartExperiment()
    {
        // Clear GUI
        _graphValues.Points.Clear();
        ExperimentGraph.ResetAllAxes();
        ExperimentGraph.InvalidatePlot(true);

        IsUiEnabled = false;
        // Start experiment
        for (_currentHairdressers = HairdressersMinCount;
             _currentHairdressers <= _hairdressersMaxCount;
             _currentHairdressers++)
        {
            _beautySalonSimulation.InitializeSimulation(SimulationTimeout, ReceptionistsCount, _currentHairdressers,
                FaceCaresCount);
            await _beautySalonSimulation.StartSimulationAsync(ReplicationsCount);

            if (_beautySalonSimulation.Cts?.Token.IsCancellationRequested ?? false)
                break;
        }

        IsUiEnabled = true;
    }

    public void ToggleExperimentPause()
    {
        _beautySalonSimulation.TogglePauseSimulation();
    }

    public void StopExperiment()
    {
        _beautySalonSimulation.StopSimulation();
    }

    private bool CanRefreshSimulationGui()
    {
        var nowTicks = DateTime.Now.Ticks;

        if (!(nowTicks > _lastGuiUpdateTime + MinTimeBetweenGuiUpdate) &&
            !_beautySalonSimulation.IsLastReplication)
            return false;

        _lastGuiUpdateTime = nowTicks;
        return true;
    }

    private void RefreshGui()
    {
        if (!CanRefreshSimulationGui()) return;

        var ci = _beautySalonSimulation.SimulationToCheckInQueueLengthAtClosingStat.ConfidenceInterval90;

        var newPoint = new ScatterErrorPoint(_currentHairdressers, ci.Mean, 0, ci.Error);

        var index = _currentHairdressers - _hairdressersMinCount;

        if (_graphValues.Points.Count == index + 1)
            _graphValues.Points[index] = newPoint;
        else
            _graphValues.Points.Add(newPoint);

        ExperimentGraph.InvalidatePlot(true);
    }

    public void Dispose()
    {
        _beautySalonSimulation.OnAfterOneIteration -= RefreshGui;
    }


    protected override Task OnDeactivateAsync(bool close, CancellationToken cancellationToken)
    {
        return Task.Run(() =>
        {
            _beautySalonSimulation.StopSimulation();
            _beautySalonSimulation.OnAfterOneIteration -= RefreshGui;
        });
    }
}