﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using BeautySalon.Models;
using BeautySalon.Models.Utils;
using Caliburn.Micro;
using EventSim.Utils;

namespace BeautySalon.ViewModels;

public class BeautySalonViewModel : Screen, IDisposable
{
    private const int MinTimeBetweenGuiUpdate = 70 * 10_000; // 70 ms
    private const double SimulationTimeout = 8 * 60 * 60;

    private readonly BeautySalonSimulation _beautySalonSimulation;
    private double _lastOneIterationGuiUpdateTime = 0;
    private double _lastSimulationGuiUpdateTime = 0;
    private double _simulationTime;
    private double _slowdownPeriodSlider;
    private double _receptionEmployeesUtilization;
    private double _hairEmployeesUtilization;
    private double _faceEmployeesUtilization;
    private bool _isUiEnabled = true;
    private int _servedCustomersCount;
    private double _simulationTimeClock;
    private long _currentReplication;

    public int ReceptionistsCount { get; set; }
    public int HairdressersCount { get; set; }
    public int FaceCaresCount { get; set; }

    public BindableCollection<Employee> ReceptionEmployees { get; } = new();
    public BindableCollection<Employee> HairEmployees { get; } = new();
    public BindableCollection<Employee> FaceEmployees { get; } = new();
    public BindableCollection<Customer> AllCustomers { get; } = new();

    public BindableCollection<Customer> ToFaceQueue { get; } = new();
    public BindableCollection<Customer> ToHairQueue { get; } = new();
    public BindableCollection<Customer> ToCheckInQueue { get; } = new();
    public BindableCollection<Customer> ToPayQueue { get; } = new();

    private ConfidenceIntervalValue? _simulationTimeInSystemCi;
    private ConfidenceIntervalValue? _simulationHaircutCustomersCountCi;
    private ConfidenceIntervalValue? _simulationMakeupCustomersCountCi;
    private ConfidenceIntervalValue? _simulationFaceCleansingCustomersCountCi;
    private ConfidenceIntervalValue? _simulationHairServiceTimeCi;
    private ConfidenceIntervalValue? _simulationServedCustomersCountCi;
    private ConfidenceIntervalValue? _simulationCustomerInCheckInQueueTimeCi;
    private ConfidenceIntervalValue? _simulationModelCoolingTimeCi;
    private ConfidenceIntervalValue? _simulationToFaceQueueLengthAtClosingCi;
    private ConfidenceIntervalValue? _simulationToHairQueueLengthAtClosingCi;
    private ConfidenceIntervalValue? _simulationToCheckInQueueLengthAtClosingCi;
    private ConfidenceIntervalValue? _simulationToPayQueueLengthAtClosingCi;
    private ConfidenceIntervalValue? _oneIterationInModelCi;
    private ConfidenceIntervalValue? _oneIterationCustomerInCheckInQueueTimeCi;
    private double _oneIterationToFaceQueueLengthAtClosingMean;
    private double _oneIterationToHairQueueLengthAtClosingMean;
    private double _oneIterationToCheckInQueueLengthAtClosingMean;
    private double _oneIterationToPayQueueLengthAtClosingMean;
    private ConfidenceIntervalValue? _simulationLeftCustomersCi;
    private ConfidenceIntervalValue? _simulationTimeInSystemAtClosingCi;
    private double _oneIterationCustomerInCheckInQueueTimeMean;
    private ConfidenceIntervalValue? _simulationToHairQueueLengthAtModelEndCi;
    private double _oneIterationTimeInSystemMean;
    private ConfidenceIntervalValue? _simulationCustomerInHairQueueTimeAtClosingCi;

    public long ReplicationMaxCount { get; set; }

    public bool IsTimeVirtual => _beautySalonSimulation.IsTimeVirtual;
    public bool IsNotTimeVirtual => !_beautySalonSimulation.IsTimeVirtual;

    public bool IsUiDisabled => !IsUiEnabled;

    public bool IsUiEnabled
    {
        get => _isUiEnabled;
        set
        {
            _isUiEnabled = value;
            NotifyOfPropertyChange();
            NotifyOfPropertyChange(nameof(IsUiDisabled));
        }
    }

    public double ReceptionEmployeesUtilization
    {
        get => _receptionEmployeesUtilization;
        set
        {
            _receptionEmployeesUtilization = value;
            NotifyOfPropertyChange();
        }
    }

    public double HairEmployeesUtilization
    {
        get => _hairEmployeesUtilization;
        set
        {
            _hairEmployeesUtilization = value;
            NotifyOfPropertyChange();
        }
    }

    public double FaceEmployeesUtilization
    {
        get => _faceEmployeesUtilization;
        set
        {
            _faceEmployeesUtilization = value;
            NotifyOfPropertyChange();
        }
    }

    public double SlowdownPeriodSlider
    {
        get => _slowdownPeriodSlider;
        set
        {
            _slowdownPeriodSlider = value;
            _beautySalonSimulation.SlowdownPeriod = value;
        }
    }

    public double SimulationTime
    {
        get => _simulationTime;
        set
        {
            _simulationTime = value;
            NotifyOfPropertyChange();
        }
    }

    public int ServedCustomersCount
    {
        get => _servedCustomersCount;
        set
        {
            _servedCustomersCount = value;
            NotifyOfPropertyChange();
        }
    }

    public double SimulationTimeClock
    {
        get => _simulationTimeClock;
        set
        {
            _simulationTimeClock = value;
            NotifyOfPropertyChange();
        }
    }

    public long CurrentReplication
    {
        get => _currentReplication;
        set
        {
            _currentReplication = value;
            NotifyOfPropertyChange();
        }
    }

    public ConfidenceIntervalValue? SimulationTimeInSystemCi
    {
        get => _simulationTimeInSystemCi;
        set
        {
            _simulationTimeInSystemCi = value;
            NotifyOfPropertyChange();
        }
    }

    public ConfidenceIntervalValue? SimulationServedCustomersCountCi
    {
        get => _simulationServedCustomersCountCi;
        set
        {
            _simulationServedCustomersCountCi = value;
            NotifyOfPropertyChange();
        }
    }

    public ConfidenceIntervalValue? SimulationCustomerInCheckInQueueTimeCi
    {
        get => _simulationCustomerInCheckInQueueTimeCi;
        set
        {
            _simulationCustomerInCheckInQueueTimeCi = value;
            NotifyOfPropertyChange();
        }
    }

    public ConfidenceIntervalValue? SimulationHairServiceTimeCi
    {
        get => _simulationHairServiceTimeCi;
        set
        {
            _simulationHairServiceTimeCi = value;
            NotifyOfPropertyChange();
        }
    }

    public ConfidenceIntervalValue? SimulationHaircutCustomersCountCi
    {
        get => _simulationHaircutCustomersCountCi;
        set
        {
            _simulationHaircutCustomersCountCi = value;
            NotifyOfPropertyChange();
        }
    }

    public ConfidenceIntervalValue? SimulationMakeupCustomersCountCi
    {
        get => _simulationMakeupCustomersCountCi;
        set
        {
            _simulationMakeupCustomersCountCi = value;
            NotifyOfPropertyChange();
        }
    }

    public ConfidenceIntervalValue? SimulationFaceCleansingCustomersCountCi
    {
        get => _simulationFaceCleansingCustomersCountCi;
        set
        {
            _simulationFaceCleansingCustomersCountCi = value;
            NotifyOfPropertyChange();
        }
    }

    public ConfidenceIntervalValue? SimulationModelCoolingTimeCi
    {
        get => _simulationModelCoolingTimeCi;
        set
        {
            _simulationModelCoolingTimeCi = value;
            NotifyOfPropertyChange();
        }
    }

    public ConfidenceIntervalValue? SimulationToFaceQueueLengthAtClosingCi
    {
        get => _simulationToFaceQueueLengthAtClosingCi;
        set
        {
            _simulationToFaceQueueLengthAtClosingCi = value;
            NotifyOfPropertyChange();
        }
    }

    public ConfidenceIntervalValue? SimulationToHairQueueLengthAtClosingCi
    {
        get => _simulationToHairQueueLengthAtClosingCi;
        set
        {
            _simulationToHairQueueLengthAtClosingCi = value;
            NotifyOfPropertyChange();
        }
    }

    public ConfidenceIntervalValue? SimulationToCheckInQueueLengthAtClosingCi
    {
        get => _simulationToCheckInQueueLengthAtClosingCi;
        set
        {
            _simulationToCheckInQueueLengthAtClosingCi = value;
            NotifyOfPropertyChange();
        }
    }

    public ConfidenceIntervalValue? SimulationToPayQueueLengthAtClosingCi
    {
        get => _simulationToPayQueueLengthAtClosingCi;
        set
        {
            _simulationToPayQueueLengthAtClosingCi = value;
            NotifyOfPropertyChange();
        }
    }

    public ConfidenceIntervalValue? OneIterationInModelCi
    {
        get => _oneIterationInModelCi;
        set
        {
            _oneIterationInModelCi = value;
            NotifyOfPropertyChange();
        }
    }

    public ConfidenceIntervalValue? OneIterationCustomerInCheckInQueueTimeCi
    {
        get => _oneIterationCustomerInCheckInQueueTimeCi;
        set
        {
            _oneIterationCustomerInCheckInQueueTimeCi = value;
            NotifyOfPropertyChange();
        }
    }

    public double OneIterationToFaceQueueLengthAtClosingMean
    {
        get => _oneIterationToFaceQueueLengthAtClosingMean;
        set
        {
            _oneIterationToFaceQueueLengthAtClosingMean = value;
            NotifyOfPropertyChange();
        }
    }

    public double OneIterationToHairQueueLengthAtClosingMean
    {
        get => _oneIterationToHairQueueLengthAtClosingMean;
        set
        {
            _oneIterationToHairQueueLengthAtClosingMean = value;
            NotifyOfPropertyChange();
        }
    }

    public double OneIterationToCheckInQueueLengthAtClosingMean
    {
        get => _oneIterationToCheckInQueueLengthAtClosingMean;
        set
        {
            _oneIterationToCheckInQueueLengthAtClosingMean = value;
            NotifyOfPropertyChange();
        }
    }

    public double OneIterationToPayQueueLengthAtClosingMean
    {
        get => _oneIterationToPayQueueLengthAtClosingMean;
        set
        {
            _oneIterationToPayQueueLengthAtClosingMean = value;
            NotifyOfPropertyChange();
        }
    }

    public ConfidenceIntervalValue? SimulationLeftCustomersCi
    {
        get => _simulationLeftCustomersCi;
        set
        {
            _simulationLeftCustomersCi = value;
            NotifyOfPropertyChange();
        }
    }

    public ConfidenceIntervalValue? SimulationTimeInSystemAtClosingCi
    {
        get => _simulationTimeInSystemAtClosingCi;
        set
        {
            _simulationTimeInSystemAtClosingCi = value;
            NotifyOfPropertyChange();
        }
    }

    public double OneIterationCustomerInCheckInQueueTimeMean
    {
        get => _oneIterationCustomerInCheckInQueueTimeMean;
        set
        {
            _oneIterationCustomerInCheckInQueueTimeMean = value;
            NotifyOfPropertyChange();
        }
    }

    public ConfidenceIntervalValue? SimulationToHairQueueLengthAtModelEndCi
    {
        get => _simulationToHairQueueLengthAtModelEndCi;
        set
        {
            _simulationToHairQueueLengthAtModelEndCi = value;
            NotifyOfPropertyChange();
        }
    }

    public double OneIterationTimeInSystemMean
    {
        get => _oneIterationTimeInSystemMean;
        set
        {
            _oneIterationTimeInSystemMean = value;
            NotifyOfPropertyChange();
        }
    }

    public ConfidenceIntervalValue? SimulationCustomerInHairQueueTimeAtClosingCi
    {
        get => _simulationCustomerInHairQueueTimeAtClosingCi;
        set
        {
            _simulationCustomerInHairQueueTimeAtClosingCi = value;
            NotifyOfPropertyChange();
        }
    }

    public BeautySalonViewModel(BeautySalonSimulation beautySalonSimulation,
        ConfidenceIntervalValue? simulationToPayQueueLengthAtClosingCi)
    {
        _beautySalonSimulation = beautySalonSimulation;
        SimulationToPayQueueLengthAtClosingCi = simulationToPayQueueLengthAtClosingCi;
        _beautySalonSimulation.OnAfterEvent += RefreshOneIterationGui;
        _beautySalonSimulation.OnAfterOneIteration += RefreshSimulationGui;

        SlowdownPeriodSlider = _beautySalonSimulation.SlowdownPeriod;
        ReplicationMaxCount = 50000;
        ReceptionistsCount = 2;
        HairdressersCount = 6;
        FaceCaresCount = 6;
        CurrentReplication = 0;
    }

    public void StopSimulation()
    {
        _beautySalonSimulation.StopSimulation();
    }

    public void TogglePauseSimulation()
    {
        _beautySalonSimulation.TogglePauseSimulation();
    }

    public async void StartSimulation()
    {
        IsUiEnabled = false;
        _beautySalonSimulation.InitializeSimulation(SimulationTimeout, ReceptionistsCount, HairdressersCount,
            FaceCaresCount);
        await _beautySalonSimulation.StartSimulationAsync(ReplicationMaxCount, IsTimeVirtual);
        IsUiEnabled = true;
    }

    public void ToggleVirtualTime()
    {
        _beautySalonSimulation.ToggleVirtualSimulationTime();
        NotifyOfPropertyChange(nameof(IsTimeVirtual));
        NotifyOfPropertyChange(nameof(IsNotTimeVirtual));
    }

    private bool CanRefreshOneIterationGui()
    {
        if (_beautySalonSimulation.IsTimeVirtual)
            return false;

        var nowTicks = DateTime.Now.Ticks;

        if (!(nowTicks > _lastOneIterationGuiUpdateTime + MinTimeBetweenGuiUpdate) &&
            !_beautySalonSimulation.IsLastSimulationChange)
            return false;

        _lastOneIterationGuiUpdateTime = nowTicks;
        return true;
    }

    private bool CanRefreshSimulationGui()
    {
        var nowTicks = DateTime.Now.Ticks;

        if (!(nowTicks > _lastSimulationGuiUpdateTime + MinTimeBetweenGuiUpdate) &&
            !_beautySalonSimulation.IsLastReplication)
            return false;

        _lastSimulationGuiUpdateTime = nowTicks;
        return true;
    }

    private void RefreshOneIterationGui()
    {
        if (!CanRefreshOneIterationGui()) return;

        // GUI update rountime:
        RefreshReceptionEmployees();
        RefreshAllCustomers();
        RefreshHairEmployees();
        RefreshFaceEmployees();
        RefreshCustomerQueues();

        ReceptionEmployeesUtilization = _beautySalonSimulation.GetReceptionEmployeesUtilization();
        HairEmployeesUtilization = _beautySalonSimulation.GetHairEmployeesUtilization();
        FaceEmployeesUtilization = _beautySalonSimulation.GetFaceEmployeesUtilization();

        CurrentReplication = _beautySalonSimulation.CurrentIteration;
        SimulationTime = _beautySalonSimulation.Time;
        SimulationTimeClock = _beautySalonSimulation.Time + BeautySalonSimulation.ClockSecondsAtStart;
        ServedCustomersCount = _beautySalonSimulation.OneIterationServedCustomersCount;

        OneIterationInModelCi = _beautySalonSimulation.OneIterationTimeInSystemStat.ConfidenceInterval90;
        OneIterationCustomerInCheckInQueueTimeCi =
            _beautySalonSimulation.OneIterationCustomerInCheckInQueueTimeStat.ConfidenceInterval90;

        OneIterationToFaceQueueLengthAtClosingMean = _beautySalonSimulation.OneIterationToFaceQueueLengthStat.Mean();
        OneIterationToHairQueueLengthAtClosingMean = _beautySalonSimulation.OneIterationToHairQueueLengthStat.Mean();
        OneIterationToCheckInQueueLengthAtClosingMean =
            _beautySalonSimulation.OneIterationToCheckInQueueLengthStat.Mean();
        OneIterationToPayQueueLengthAtClosingMean = _beautySalonSimulation.OneIterationToPayQueueLengthStat.Mean();

        OneIterationCustomerInCheckInQueueTimeMean =
            _beautySalonSimulation.OneIterationCustomerInCheckInQueueTimeStat.Mean();

        OneIterationTimeInSystemMean = _beautySalonSimulation.OneIterationTimeInSystemStat.Mean();
    }

    private void RefreshSimulationGui()
    {
        if (!CanRefreshSimulationGui()) return;

        CurrentReplication = _beautySalonSimulation.CurrentIteration;

        SimulationTimeInSystemCi = _beautySalonSimulation.SimulationTimeInSystemStat.ConfidenceInterval90;
        SimulationTimeInSystemAtClosingCi =
            _beautySalonSimulation.SimulationTimeInSystemAtClosingStat.ConfidenceInterval90;

        SimulationHairServiceTimeCi =
            _beautySalonSimulation.SimulationHairServiceTimeStat.ConfidenceInterval90;

        SimulationHaircutCustomersCountCi =
            _beautySalonSimulation.SimulationHaircutCustomersCountStat.ConfidenceInterval90;
        SimulationMakeupCustomersCountCi =
            _beautySalonSimulation.SimulationMakeupCustomersCountStat.ConfidenceInterval90;
        SimulationFaceCleansingCustomersCountCi =
            _beautySalonSimulation.SimulationFaceCleansingCustomersCountStat.ConfidenceInterval90;
        SimulationServedCustomersCountCi =
            _beautySalonSimulation.SimulationServedCustomersCountStat.ConfidenceInterval90;
        SimulationCustomerInCheckInQueueTimeCi =
            _beautySalonSimulation.SimulationCustomerInCheckInQueueTimeStat.ConfidenceInterval90;

        SimulationToFaceQueueLengthAtClosingCi =
            _beautySalonSimulation.SimulationToFaceQueueLengthAtClosingStat.ConfidenceInterval90;
        SimulationToHairQueueLengthAtClosingCi =
            _beautySalonSimulation.SimulationToHairQueueLengthAtClosingStat.ConfidenceInterval90;
        SimulationToCheckInQueueLengthAtClosingCi =
            _beautySalonSimulation.SimulationToCheckInQueueLengthAtClosingStat.ConfidenceInterval90;
        SimulationToPayQueueLengthAtClosingCi =
            _beautySalonSimulation.SimulationToPayQueueLengthAtClosingStat.ConfidenceInterval90;

        SimulationLeftCustomersCi = _beautySalonSimulation.SimulationLeftCustomersStat.ConfidenceInterval90;

        SimulationModelCoolingTimeCi = _beautySalonSimulation.SimulationModelCoolingTimeStat.ConfidenceInterval90;

        SimulationToHairQueueLengthAtModelEndCi =
            _beautySalonSimulation.SimulationToHairQueueLengthAtModelEndStat.ConfidenceInterval90;

        SimulationCustomerInHairQueueTimeAtClosingCi =
            _beautySalonSimulation.SimulationCustomerInHairQueueTimeAtClosingStat.ConfidenceInterval90;
    }

    private void RefreshCustomerQueues()
    {
        ToFaceQueue.IsNotifying = false;
        ToHairQueue.IsNotifying = false;
        ToCheckInQueue.IsNotifying = false;
        ToPayQueue.IsNotifying = false;

        ToFaceQueue.Clear();
        ToHairQueue.Clear();
        ToCheckInQueue.Clear();
        ToPayQueue.Clear();

        ToFaceQueue.IsNotifying = true;
        ToHairQueue.IsNotifying = true;
        ToCheckInQueue.IsNotifying = true;
        ToPayQueue.IsNotifying = true;

        ToFaceQueue.AddRange(_beautySalonSimulation.ToFaceQueue);
        ToHairQueue.AddRange(_beautySalonSimulation.ToHairQueue);
        ToCheckInQueue.AddRange(_beautySalonSimulation.ToCheckInQueue);
        ToPayQueue.AddRange(_beautySalonSimulation.ToPayQueue);
    }

    private void RefreshAllCustomers()
    {
        AllCustomers.IsNotifying = false;
        AllCustomers.Clear();
        AllCustomers.IsNotifying = true;
        AllCustomers.AddRange(_beautySalonSimulation.AllCustomers);
    }

    private void RefreshReceptionEmployees()
    {
        ReceptionEmployees.IsNotifying = false;
        ReceptionEmployees.Clear();
        ReceptionEmployees.IsNotifying = true;
        ReceptionEmployees.AddRange(_beautySalonSimulation.ReceptionEmployeesStable);
    }

    private void RefreshHairEmployees()
    {
        HairEmployees.IsNotifying = false;
        HairEmployees.Clear();
        HairEmployees.IsNotifying = true;
        HairEmployees.AddRange(_beautySalonSimulation.HairEmployeesStable);
    }

    private void RefreshFaceEmployees()
    {
        FaceEmployees.IsNotifying = false;
        FaceEmployees.Clear();
        FaceEmployees.IsNotifying = true;
        FaceEmployees.AddRange(_beautySalonSimulation.FaceEmployeesStable);
    }

    public void Dispose()
    {
        _beautySalonSimulation.OnAfterEvent -= RefreshOneIterationGui;
        _beautySalonSimulation.OnAfterOneIteration -= RefreshSimulationGui;
    }

    protected override Task OnDeactivateAsync(bool close, CancellationToken cancellationToken)
    {
        return Task.Run(() =>
        {
            _beautySalonSimulation.StopSimulation();
            _beautySalonSimulation.OnAfterEvent -= RefreshOneIterationGui;
            _beautySalonSimulation.OnAfterOneIteration -= RefreshSimulationGui;
        });
    }
}