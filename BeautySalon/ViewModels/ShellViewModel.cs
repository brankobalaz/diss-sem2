﻿using System.Windows.Controls;
using BeautySalon.Models.Utils;
using Caliburn.Micro;

namespace BeautySalon.ViewModels;

public class ShellViewModel : Screen
{
    private readonly SimpleContainer _container;
    private INavigationService? _navigationService;

    public ShellViewModel(SimpleContainer container)
    {
        _container = container;

        Tools = new BindableCollection<ViewModelShell>
        {
            new("Simulácia", "Umožňuje pozorovať priebeh simulácie.", typeof(BeautySalonViewModel)),
            new("Experiment", "Experiment závislosti", typeof(HairdressersExperimentViewModel))
        };
    }

    public BindableCollection<ViewModelShell> Tools { get; }

    public void RegisterFrame(Frame frame)
    {
        _navigationService = new FrameAdapter(frame);
        _container.Instance(_navigationService);

        _navigationService.NavigateToViewModel(typeof(BeautySalonViewModel));
    }

    public void ShowViewModel(ViewModelShell viewModelShell)
    {
        _navigationService!.NavigateToViewModel(viewModelShell.ViewModel);
    }
}