﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace BeautySalon.Views.Converters;

public class SecondsToClockConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        var seconds = (double) value;

        var days = Math.Floor(seconds / (24 * 60 * 60));
        seconds -= days * (24 * 60 * 60);
        var hours = Math.Floor(seconds / (60 * 60));
        seconds -= hours * (60 * 60);
        var minutes = Math.Floor(seconds / 60);
        seconds -= minutes * 60;

        var res = "";
        res += days != 0 ? $"+{days} d " : "";
        res += $"{hours:00}:{minutes:00}:{seconds:00}";

        return res;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}