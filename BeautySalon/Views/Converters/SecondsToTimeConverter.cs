﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace BeautySalon.Views.Converters;

public class SecondsToTimeConverter : IValueConverter
{
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        var seconds = (double) value;

        if (double.IsNaN(seconds))
            return "NaN";

        if (seconds < 0)
            return "NeG";

        var days = Math.Floor(seconds / (24 * 60 * 60));
        seconds -= days * (24 * 60 * 60);
        var hours = Math.Floor(seconds / (60 * 60));
        seconds -= hours * (60 * 60);
        var minutes = Math.Floor(seconds / 60);
        seconds -= minutes * 60;

        var res = "";
        res += days != 0 ? $"{days}d " : "";
        res += hours != 0 ? $"{hours}h " : "";
        res += minutes != 0 ? $"{minutes}m " : "";
        res += $"{seconds:F2}s";

        return res;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}