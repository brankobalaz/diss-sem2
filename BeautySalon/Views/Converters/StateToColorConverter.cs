﻿using System;
using System.Globalization;
using System.Windows.Data;
using BeautySalon.Models.Utils;

namespace BeautySalon.Views.Converters;

public class StateToColorConverter : IValueConverter
{
    #region IValueConverter Members

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        var state = (ProcedureState) value;

        switch (state)
        {
            case ProcedureState.NotRequested:
                return "LightGray";
            case ProcedureState.Requested:
                return "Yellow";
            case ProcedureState.InProgress:
                return "DodgerBlue";
            case ProcedureState.Done:
                return "Green";
            default:
                return "Black";
        }
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }

    #endregion
}