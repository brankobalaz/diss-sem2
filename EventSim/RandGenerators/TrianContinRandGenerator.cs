﻿namespace EventSim.RandGenerators;

public class TrianContinRandGenerator : RandGenerator
{
    private readonly double _fc;
    private readonly double _max;
    private readonly double _mean;
    private readonly double _min;
    private readonly Random _rand;

    public TrianContinRandGenerator(double min, double mean, double max, int seed = -1)
    {
        _min = min;
        _mean = mean;
        _max = max;
        _fc = (mean - min) / (max - min);
        _rand = seed == -1 ? new Random() : new Random(seed);
    }


    public override double Next()
    {
        var u = _rand.NextDouble();

        if (u < _fc)
            return _min + Math.Sqrt(u * (_max - _min) * (_mean - _min));

        return _max - Math.Sqrt((1 - u) * (_max - _min) * (_max - _mean));
    }
}