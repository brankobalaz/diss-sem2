﻿using EventSim.RandGenerators.Utils;

namespace EventSim.RandGenerators;

public class EmpirDiscrRandGenerator : RandGenerator
{
    private readonly List<double> _cumulativeProbability = new();
    private readonly Random _rand;
    private readonly List<UniDiscrRandGenerator> _randoms = new();

    public EmpirDiscrRandGenerator(List<EmpirDiscrRandAttribute> attributes, Random? seedGenerator = null)
    {
        _rand = seedGenerator is null ? new Random() : new Random(seedGenerator.Next());

        for (var i = 0; i < attributes.Count; i++)
        {
            var attribute = attributes[i];

            _randoms.Add(new UniDiscrRandGenerator(attribute.Min, attribute.Max, seedGenerator?.Next() ?? -1));

            if (i == 0)
                _cumulativeProbability.Add(attribute.Probability);
            else
                _cumulativeProbability.Add(_cumulativeProbability[i - 1] + attribute.Probability);
        }

        if (_cumulativeProbability[^1] > 1)
            throw new ArgumentException("Probability is greater than 1!");
    }

    public override double Next()
    {
        var u = _rand.NextDouble();

        for (var i = 0; i < _randoms.Count; i++)
            if (u < _cumulativeProbability[i])
                return _randoms[i].Next();

        throw new ArgumentException("Something went wrong...");
    }
}