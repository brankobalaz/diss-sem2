﻿namespace EventSim.RandGenerators;

public abstract class RandGenerator
{
    public abstract double Next();
}