﻿namespace EventSim.RandGenerators;

public class UniDiscrRandGenerator : RandGenerator
{
    private readonly double _min;
    private readonly Random _rand;
    private readonly double _range;

    /// <summary>
    ///     Generates numbers in [min, max] range.
    ///     MAX is inclusive!
    /// </summary>
    public UniDiscrRandGenerator(double min, double max, int seed = -1)
    {
        _min = min;
        _range = max - min + 1;
        _rand = seed == -1 ? new Random() : new Random(seed);
    }

    public override double Next()
    {
        return _min + (int) (_rand.NextDouble() * _range);
    }
}