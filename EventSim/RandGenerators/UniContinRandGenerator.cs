﻿namespace EventSim.RandGenerators;

public class UniContinRandGenerator : RandGenerator
{
    private readonly int _min;
    private readonly Random _rand;
    private readonly int _range;

    public UniContinRandGenerator(int min, int max, int seed = -1)
    {
        _min = min;
        _range = max - min;
        _rand = seed == -1 ? new Random() : new Random(seed);
    }

    public override double Next()
    {
        return _min + _rand.NextDouble() * _range;
    }
}