﻿namespace EventSim.RandGenerators.Utils;

public class EmpirDiscrRandAttribute
{
    public EmpirDiscrRandAttribute(double min, double max, double probability)
    {
        Min = min;
        Max = max;
        Probability = probability;
    }

    public double Min { get; }
    public double Max { get; }
    public double Probability { get; }
}