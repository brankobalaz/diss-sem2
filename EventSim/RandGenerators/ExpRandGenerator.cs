﻿namespace EventSim.RandGenerators;

public class ExpRandGenerator : RandGenerator
{
    private readonly double _lambda;
    private readonly Random _rand;

    public ExpRandGenerator(double lambda, int seed = -1)
    {
        _lambda = lambda;
        _rand = seed == -1 ? new Random() : new Random(seed);
    }

    public override double Next()
    {
        return -Math.Log(1 - _rand.NextDouble()) / _lambda;
    }
}