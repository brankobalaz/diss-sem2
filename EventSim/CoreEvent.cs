﻿namespace EventSim;

public abstract class CoreEvent : IComparable<CoreEvent>
{
    public double Time { get; }
    private int Priority { get; }

    protected CoreEvent(double time, int priority)
    {
        Time = time;
        Priority = priority;
    }

    public abstract void Execute();

    public int CompareTo(CoreEvent? other)
    {
        var cmp1 = Time.CompareTo(other!.Time);
        return cmp1 != 0 ? cmp1 : Priority.CompareTo(other.Priority);
    }
}