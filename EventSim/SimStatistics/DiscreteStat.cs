﻿using EventSim.Utils;

namespace EventSim.SimStatistics;

public class DiscreteStat : Statistics
{
    private const double CriticalValue90 = 1.6448539495468137;
    private const double CriticalValue95 = 1.9599641561508176;
    private const double CriticalValue99 = 2.5758293867111206;

    private double _sum;
    private double _sum2;
    private double _samplesCount;

    public void Add(double value)
    {
        _sum += value;
        _sum2 += value * value;
        _samplesCount++;
    }

    public override double Mean()
    {
        return _samplesCount == 0 ? -1 : _sum / _samplesCount;
    }

    private double StandardDeviation()
    {
        var fraction = 1 / (_samplesCount - 1);
        return Math.Sqrt(fraction * _sum2 - Math.Pow(fraction * _sum, 2));
    }

    private ConfidenceIntervalValue ConfidenceInterval(double criticalValue, double percent)
    {
        var average = _sum / _samplesCount;

        if (_samplesCount < 30)
            return new ConfidenceIntervalValue(double.NaN, double.NaN, average, 0, percent);

        var numerator = StandardDeviation() * criticalValue;
        var denominator = Math.Sqrt(_samplesCount);
        var fraction = numerator / denominator;

        return new ConfidenceIntervalValue(average - fraction, average + fraction, average, fraction, percent);
    }

    public ConfidenceIntervalValue ConfidenceInterval90 => ConfidenceInterval(CriticalValue90, 90);
    public ConfidenceIntervalValue ConfidenceInterval95 => ConfidenceInterval(CriticalValue95, 95);
    public ConfidenceIntervalValue ConfidenceInterval99 => ConfidenceInterval(CriticalValue99, 99);

    public override void Clear()
    {
        _sum = 0;
        _sum2 = 0;
        _samplesCount = 0;
    }
}