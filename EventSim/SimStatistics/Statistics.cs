﻿namespace EventSim.SimStatistics;

public abstract class Statistics
{
    public abstract double Mean();

    public abstract void Clear();
}