﻿namespace EventSim.SimStatistics;

public class ContinuousStat : Statistics
{
    private double _sum;
    private double _lastUpdateTime;
    private double _lastValue;

    public ContinuousStat(double lastUpdateTime = 0)
    {
        _lastUpdateTime = lastUpdateTime;
    }

    public void Add(double value, double time)
    {
        var weight = time - _lastUpdateTime;
        _sum += weight * _lastValue;
        
        _lastUpdateTime = time;
        _lastValue = value;
    }

    public override double Mean()
    {
        return _lastUpdateTime == 0 ? -1 : _sum / _lastUpdateTime;
    }

    public double Mean(double time)
    {
        return _lastUpdateTime == 0 ? -1 : (_sum + (time - _lastUpdateTime) * _lastValue) / time;
    }

    public override void Clear()
    {
        _sum = 0;
        _lastUpdateTime = 0;
        _lastValue = 0;
    }
}