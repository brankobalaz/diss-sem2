﻿using C5;
using EventSim.Utils;

namespace EventSim;

public abstract class EventSimCore
{
    private readonly IntervalHeap<CoreEvent> _simEventCalendar = new();
    public CancellationTokenSource? Cts { get; private set; }
    private readonly EventWaitHandle _waitHandle = new ManualResetEvent(true);
    private bool _isPause = false;

    public double Time { get; protected set; }

    public long CurrentIteration { get; private set; }
    public long MaxIterations { get; private set; }

    public bool IsLastReplication { get; private set; }
    public bool IsLastSimulationChange { get; private set; }

    public bool IsTimeVirtual { get; set; }
    public int SlowdownTimeMs { get; set; } = 100;
    public double SlowdownPeriod { get; set; } = 200;

    public void AddEventToCalendar(CoreEvent newEvent)
    {
        _simEventCalendar.Add(newEvent);
    }

    /// <summary>
    /// One replication code
    /// </summary>
    private void OneReplication(CancellationToken token)
    {
        while (_simEventCalendar.Count > 0 && !token.IsCancellationRequested)
        {
            var simEvent = _simEventCalendar.DeleteMin();

            if (simEvent.Time < Time)
                throw new Exception("Time is moving backwards!");

            Time = simEvent.Time;
            simEvent.Execute();

            if (!IsTimeVirtual)
                if (_simEventCalendar.Count == 1)
                    if (_simEventCalendar.FindMin() is SlowdownEvent)
                        _simEventCalendar.DeleteMin();

            if (_simEventCalendar.IsEmpty)
                IsLastSimulationChange = true;

            AfterEventExecute();

            _waitHandle.WaitOne();
        }
    }

    /// <summary>
    /// Initialize randomness
    /// </summary>
    protected virtual void BeforeSimulation()
    {
    }

    /// <summary>
    /// Nessesary cleanup
    /// </summary>
    protected virtual void AfterSimulation()
    {
    }

    /// <summary>
    /// Handle replication result
    /// </summary>
    protected virtual void AfterOneReplication()
    {
    }

    /// <summary>
    /// Initialization before replication
    /// </summary>
    protected virtual void BeforeOneReplication()
    {
    }

    /// <summary>
    /// Start to simulate
    /// </summary>
    public Task StartSimulationAsync(long replicationCount, bool isTimeVirtual = true)
    {
        IsTimeVirtual = isTimeVirtual;
        MaxIterations = replicationCount;
        IsLastReplication = false;
        Cts = new CancellationTokenSource();
        _isPause = false;
        var token = Cts.Token;

        return Task.Run(() =>
        {
            BeforeSimulation();
            for (CurrentIteration = 1; CurrentIteration <= replicationCount; CurrentIteration++)
            {
                IsLastSimulationChange = false;
                while (!_simEventCalendar.IsEmpty) // Perform calendar clear
                    _simEventCalendar.DeleteMin();

                BeforeOneReplication();

                if (!IsTimeVirtual)
                    AddEventToCalendar(new SlowdownEvent(0, this));

                OneReplication(token);

                if (token.IsCancellationRequested || CurrentIteration == replicationCount)
                    IsLastReplication = true;

                AfterOneReplication();

                if (token.IsCancellationRequested)
                    break;
            }

            AfterSimulation();
        }, token);
    }

    /// <summary>
    /// Initialize randomness
    /// </summary>
    protected virtual void AfterEventExecute()
    {
    }

    /// <summary>
    /// Abort simulation before replicationCount is hit
    /// </summary>
    public void StopSimulation()
    {
        Cts?.Cancel();
        _waitHandle.Set();
    }

    public void TogglePauseSimulation()
    {
        _isPause = !_isPause;

        if (_isPause)
            _waitHandle.Reset();
        else
            _waitHandle.Set();
    }

    public void ToggleVirtualSimulationTime()
    {
        IsTimeVirtual = !IsTimeVirtual;
    }
}