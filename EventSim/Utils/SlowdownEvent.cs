﻿namespace EventSim.Utils;

public class SlowdownEvent : CoreEvent
{
    private EventSimCore _simulation;

    public SlowdownEvent(double time, EventSimCore simulation) : base(time, int.MaxValue)
    {
        _simulation = simulation;
    }

    public override void Execute()
    {
        Thread.Sleep(_simulation.SlowdownTimeMs);

        if (!_simulation.IsTimeVirtual)
        {
            var nextEventTime = _simulation.Time + _simulation.SlowdownPeriod;
            _simulation.AddEventToCalendar(new SlowdownEvent(nextEventTime, _simulation));
        }
    }
}