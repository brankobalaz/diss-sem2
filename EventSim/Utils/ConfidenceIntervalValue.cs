﻿namespace EventSim.Utils;

public class ConfidenceIntervalValue
{
    public double Min { get; }
    public double Max { get; }
    public double Mean { get; }
    public double Error { get; }
    public double Percent { get; }

    public ConfidenceIntervalValue(double min, double max, double mean, double error, double percent)
    {
        Min = min;
        Max = max;
        Mean = mean;
        Error = error;
        Percent = percent;
    }
}